"""Module for computing sqrt with newton's method"""

def sqrt2(a):
    """Function for computing sqrt with Newton's method"""

    assert type(a) is int or type(a) is float "error, input must be numeric"
    x0 =1
    tol = 1e-12
    maxit = 10000

    for i in range(maxit):
        x = x0/2 + a/(2*x0)
        delta_x = abs(x-x0)
        print("after iteration %d, x= %18.16f" %(i+1,x))
        if delta_x<tol:
            print("converged")
            break
        x0 = x

    return x
