"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=0)
    Xv = np.var(X,axis=0)
    Xsv = np.sqrt(Xv)

    plt.figure()
    for i in range(M):
        plt.plot(X[i,:])
    plt.show()

    plt.figure()
    plt.xlabel('time')
    plt.ylabel('standard deviation')

    plt.plot(Xsv)

    plt.figure()
    return X,Xm,Xv


def analyze(display=False):
    Xtj = np.var(X[:,Nt])
    ep = abs(Nt - Xtj)

    if display==True:
        plt.figure()
        plt.xlabel('Mvalues')
        plt.ylabel('error')

        plt.loglog(M,ep)
    return M, Xtj, ep
    """Complete this function to analyze simulation error
    """
